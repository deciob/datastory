var express = require('express');
var app = express();

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
};

app.use(allowCrossDomain);

app.get('/data', function (req, res) {

  var payload = [
    {
      header: {
        title: 'Hello app - page 1',
      },
      graph: {
        data: [],
        config: {},
      },
      narrative: {
        title: 'Page 1 narrative',
        content: 'stuff',
      },
    },
    {
      header: {
        title: 'Hello app - page 2',
      },
      graph: {
        data: [],
        config: {},
      },
      narrative: {
        title: 'Page 2 narrative',
        content: 'stuff',
      },
    }
  ];

  res.send(payload);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
