export const SET_DATA = 'SET_DATA';

export function setData(data) {
  return {
    type: SET_DATA,
    payload: data,
  };
}

let dataStore;

function fetchData() {
  return fetch('http://192.168.1.64:3000/data', {
    mode: 'cors'
  });
}

export function loadData() {
  // Invert control!
  // Return a function that accepts `dispatch` so we can dispatch later.
  // Thunk middleware knows how to turn thunk async actions into actions.

  return function (dispatch, getState) {
    const currentDataIdx = getState().get('controls').get('currentDataIdx');
    if (dataStore) {
      return dispatch(setData(dataStore[currentDataIdx]));
    }
    return fetchData().then(r => r.json()).then(
      data => {
        dataStore = data;
        dispatch(setData(data[currentDataIdx]))
      }//,
      //error => dispatch(apologize('The Sandwich Shop', forPerson, error))
    );
  };
}
