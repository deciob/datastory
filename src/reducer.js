import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  data: {
    header: {
      title: 'Hello!',
    },
    graph: {
      data: [],
      config: {},
    },
    narrative: {
      title: 'Narrative',
      content: '...',
    },
  },
  controls: {
    currentDataIdx: 0,
  },
});

function setData(state, _newState) {
  const newDataState = Immutable.fromJS(_newState);
  return state.set('data', newDataState);
}

function setDataIdx(state, newState) {
  return state.setIn(['controls', 'currentDataIdx'], newState);
}

// TODO: Splitting Reducers http://redux.js.org/docs/basics/Reducers.html
export default function (state = initialState, action) {
  switch (action.type) {
    case 'SET_DATA':
      return setData(state, action.payload);
    case 'SET_DATA_IDX':
      return setDataIdx(state, action.payload);
    default:
      return state;
  }
}
