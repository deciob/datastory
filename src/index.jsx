// require('./main.css');

import thunk from 'redux-thunk';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducer from './reducer';
import { DatastoryContainer } from './components/Datastory';
import { loadData } from './action-creators';

// const store = createStore(reducer);
const store = createStore(
  reducer,
  applyMiddleware(thunk)
);

store.dispatch(
  loadData()
).then(() => {
  console.log('Done!');
});


ReactDOM.render(
  <Provider store={store}>
    <DatastoryContainer />
  </Provider>,
  document.getElementById('app')
);
