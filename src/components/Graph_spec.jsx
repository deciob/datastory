import Immutable from 'immutable';
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Graph from './Graph';

describe('Graph', () => {
  it('renders a graph', () => {
    const graph = Immutable.Map({ title: 'Hello Graph!' });
    const wrapper = shallow(<Graph graph={graph} />);
    const title = wrapper.find('h2');

    expect(title.text()).to.equal('Hello Graph!');
  });
});
