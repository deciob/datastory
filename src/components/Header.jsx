import ImmutablePropTypes from 'react-immutable-proptypes';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import React from 'react';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  getTitle() {
    return this.props.header.get('title');
  }
  render() {
    return (
      <div className="header-section panel">
        <h1 className="title">Data Story Sample App</h1>
        <h3 className="title">{this.getTitle()}</h3>
      </div>
    );
  }
}

Header.propTypes = {
  header: ImmutablePropTypes.map,
};
// Header.defaultProps = { title: 'First story' };

export default Header;
