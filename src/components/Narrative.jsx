import ImmutablePropTypes from 'react-immutable-proptypes';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import React from 'react';

class Narrative extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  getTitle() {
    return this.props.narrative.get('title');
  }
  getContent() {
    return this.props.narrative.get('content');
  }
  render() {
    return (
      <div className="narrative-section panel">
        <h2 className="title narrative-title">{this.getTitle()}</h2>
        <p className="content narrative-content">{this.getContent()}</p>
      </div>
    );
  }
}

Narrative.propTypes = {
  narrative: ImmutablePropTypes.map,
};
// Narrative.defaultProps = {
//   content: '...',
//   title: 'Narrative',
// };

export default Narrative;
