import Immutable from 'immutable';
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Narrative from './Narrative';

describe('Narrative', () => {
  it('renders a narrative', () => {
    const narrativeModel = Immutable.Map({
      title: 'title',
      content: 'content',
    });
    const wrapper = shallow(<Narrative narrative={narrativeModel} />);
    const title = wrapper.find('.narrative-title');
    const content = wrapper.find('.narrative-content');

    expect(title.text()).to.equal('title');
    expect(content.text()).to.equal('content');
  });
});
