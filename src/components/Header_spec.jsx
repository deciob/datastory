import Immutable from 'immutable';
import React from 'react';
import ReactDOM from 'react-dom';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Map } from 'immutable';
import { scryRenderedDOMComponentsWithTag } from 'react-addons-test-utils';

import Header from './Header';

describe('Header', () => {
  it('renders a header', () => {
    const header = Immutable.Map({ title: 'Hello Header!' });
    const wrapper = shallow(<Header header={header} />);
    const title = wrapper.find('h3');

    expect(title.text()).to.equal('Hello Header!');
  });

  it('does update DOM when prop changes', () => {
    const props = new Map({ title: 'decio and leftraru' });
    const container = document.createElement('div');
    let component = ReactDOM.render(
      <Header header={props} />,
      container
    );
    let title = scryRenderedDOMComponentsWithTag(component, 'h3')[0];
    expect(title.textContent).to.equal('decio and leftraru');

    const newProps = props.set('title', 'decio and leftraru and gaby and rayen');

    component = ReactDOM.render(
      <Header header={newProps} />,
      container
    );
    title = scryRenderedDOMComponentsWithTag(component, 'h3')[0];
    expect(title.textContent).to.equal('decio and leftraru and gaby and rayen');
  });
});
