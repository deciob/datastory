import Immutable from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import React from 'react';
import { connect } from 'react-redux';

import Controls from './Controls';
import Graph from './Graph';
import Header from './Header';
import Narrative from './Narrative';

export class Datastory extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  getHeader() {
    return this.props.model.get('data').get('header');
  }
  getGraph() {
    return this.props.model.get('data').get('graph');
  }
  getNarrative() {
    return this.props.model.get('data').get('narrative');
  }
  render() {
    return (
      <div className="main-section">
        <Header header={this.getHeader()} />
        <div className="body-section">
          <Graph graph={this.getGraph()} />
          <div className="side-content">
            <Narrative narrative={this.getNarrative()} />
            <Controls />
          </div>
        </div>
      </div>
    );
  }
}

Datastory.propTypes = {
  model: ImmutablePropTypes.map,
  header: ImmutablePropTypes.map,
  graph: ImmutablePropTypes.map,
  narrative: ImmutablePropTypes.map,
};

// Datastory.defaultProps = {
//   model: Immutable.Map({
//     data: Immutable.Map(),
//   }),
// };

function mapStateToProps(state) {
  return {
    model: state,
  };
}

export const DatastoryContainer = connect(mapStateToProps)(Datastory);
