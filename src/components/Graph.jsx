import ImmutablePropTypes from 'react-immutable-proptypes';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import React from 'react';

class Graph extends React.Component {
  constructor(props) {
    super(props);
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
  }
  getTitle() {
    return this.props.graph.get('title');
  }
  render() {
    return (
      <div className="graph-section panel">
        <h2>{this.getTitle()}</h2>
        <div className="graph"></div>
      </div>
    );
  }
}

Graph.propTypes = {
  graph: ImmutablePropTypes.map,
};
// Graph.defaultProps = { title: 'Graph title' };

export default Graph;
