import Immutable from 'immutable';
import { expect } from 'chai';

import reducer from '../src/reducer';

describe('reducer', () => {
  it('handles SET_DATA with plain JS payload', () => {
    const initialState = Immutable.Map({
      data: Immutable.Map(),
      controls: Immutable.Map({
        currentDataIdx: 0,
      }),
    });
    const action = {
      type: 'SET_DATA',
      payload: {
        header: {
          title: 'Hello app!',
        },
        graph: {
          data: [],
          config: {},
        },
        narrative: {
          title: 'Narrative',
          content: '...',
        },
      },
    };
    const nextState = reducer(initialState, action);

    expect(nextState).to.equal(Immutable.fromJS({
      data: {
        header: {
          title: 'Hello app!',
        },
        graph: {
          data: [],
          config: {},
        },
        narrative: {
          title: 'Narrative',
          content: '...',
        },
      },
      controls: {
        currentDataIdx: 0,
      },
    }));
  });

  it('handles SET_DATA without initial data', () => {
    const initialState = Immutable.Map({
      data: void 0,
      controls: Immutable.Map({
        currentDataIdx: 0,
      }),
    });
    const action = {
      type: 'SET_DATA',
      payload: {
        header: {
          title: 'Hello app!',
        },
        graph: {
          data: [],
          config: {},
        },
        narrative: {
          title: 'Narrative',
          content: '...',
        },
      },
    };
    const nextState = reducer(initialState, action);

    expect(nextState).to.equal(Immutable.fromJS({
      data: {
        header: {
          title: 'Hello app!',
        },
        graph: {
          data: [],
          config: {},
        },
        narrative: {
          title: 'Narrative',
          content: '...',
        },
      },
      controls: {
        currentDataIdx: 0,
      },
    }));
  });

  it('handles SET_DATA_IDX with plain JS payload', () => {
    const initialState = Immutable.Map({
      data: Immutable.Map(),
      controls: Immutable.Map({
        currentDataIdx: 0,
      }),
    });
    const action = {
      type: 'SET_DATA_IDX',
      payload: 1,
    };
    const nextState = reducer(initialState, action);

    expect(nextState).to.equal(Immutable.fromJS({
      data: {},
      controls: {
        currentDataIdx: 1,
      },
    }));
  });
});
